server {
    listen              80;
    server_name         _;

    # security
    # include             conf.d/security.conf;

    location /static {
        alias /app/astra/static;
    }

    # logging
    access_log /var/log/nginx/access.log combined buffer=512k flush=1m;
    error_log  /var/log/nginx/error.log warn;

    # reverse proxy
    location / {
        proxy_pass            http://${APP_HOST}:${APP_PORT};
        proxy_set_header Host $host;
    }

    # additional config
    # include conf.d/general.conf;
}

# HTTP redirect
server {
    listen      80;
    server_name ._;
    return      301 https://$host$request_uri;
}