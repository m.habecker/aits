from django.test import TestCase
from django.contrib.auth import get_user_model

from .models import UserProfile, dinoName, strikeType, banLength, Rule, revTicket, punishTicket

class AdminUserTestCase(TestCase):
    def setUp(self):
        self.UserModel = get_user_model()

    def test_create_superuser(self):
        # Create a superuser
        admin_user = self.UserModel.objects.create_superuser(
            username='admin', 
            email='admin@example.com', 
            password='testpassword123'
        )

        # Check that the superuser has been created
        self.assertEqual(self.UserModel.objects.filter(username='admin').count(), 1)

        # Check that the created user is indeed a superuser and staff
        self.assertTrue(admin_user.is_superuser)
        self.assertTrue(admin_user.is_staff)

        # Optionally, you can also check the email
        self.assertEqual(admin_user.email, 'admin@example.com')
        print('Admin user created successfully!')

    def test_create_user(self):
        # Create a normal user
        user = self.UserModel.objects.create_user(
            username='tim',
            email='tim@email.com',
            password='testpassword123'
        )

        self.assertEqual(self.UserModel.objects.filter(username='tim').count(), 1)
        self.assertEqual(user.is_superuser, False)
        self.assertEqual(user.is_staff, False)
        print('Normal user created successfully!')

class ModelTest(TestCase):
    def test_dinoName(self):
        add_dino = dinoName.objects.create(dino="Coahuilaceratops")

        self.assertEqual(dinoName.objects.filter(dino="Coahuilaceratops").count(), 1)
        print("Dino added successfully!")

    def test_strikeType(self):
        add_strike = strikeType.objects.create(strike="Strike 1")

        self.assertEqual(strikeType.objects.filter(strike="Strike 1").count(), 1)
        print("Strike added successfully!")

    def test_banLength(self):
        add_ban = banLength.objects.create(banLen="1 Day")

        self.assertEqual(banLength.objects.filter(banLen="1 Day").count(), 1)
        print("Ban length added successfully!")

    def test_Rule(self):
        add_rule = Rule.objects.create(rules="Rule 1", description="Rule 1 Description", clarity="Rule 1 Clarity")

        self.assertEqual(Rule.objects.filter(rules="Rule 1").count(), 1)
        print("Rule added successfully!")

    def test_revTicket(self):
        add_rev = revTicket.objects.create(modName="TestMod", addtlMods="TestMod2", userName="TestUser", discName="TestUser#1234", steamId=123456789, growth="Growth", dinoName="TestDino", revd=True, ticketLink="https://www.google.com", counterLink="https://www.google.com")

        self.assertEqual(revTicket.objects.filter(modName="TestMod").count(), 1)
        print("Rev ticket added successfully!")

    def test_punishTicket(self):
        add_punish = punishTicket.objects.create(modName="TestMod", userName="TestUser", steamId=123456789, punishment="Strike 1", banTime="1 Day", banTimeOther="1 Day", reason="Test Reason", ticketLink="https://www.google.com", courtesy=True, counterLink="https://www.google.com")

        self.assertEqual(punishTicket.objects.filter(modName="TestMod").count(), 1)
        print("Punish ticket added successfully!")
