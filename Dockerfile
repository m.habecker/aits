# Start with a base Python image
FROM python:3.9

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
RUN mkdir /astra

# Copy project
COPY ./astra /astra
COPY ./requirements.txt /astra/
COPY .env /astra/.env
# COPY ./scripts/run.sh /scripts/run.sh

ENV PATH="/scripts:${PATH}"

WORKDIR /astra
RUN pip install --no-cache-dir -r requirements.txt

# CMD ["run.sh"]
