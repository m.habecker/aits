#!/usr/bin/env bash

cd /var/www/aits
docker compose down

git pull

docker compose build

docker compose run --rm app python3 manage.py makemigrations
docker compose run --rm app python3 manage.py migrate
docker compose run --rm app python3 manage.py collectstatic --no-input

docker compose up -d
