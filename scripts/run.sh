#!/bin/sh

set -e

# Do all the migrations and collect static files
# python3 manage.py collectstatic --noinput
# python3 manage.py makemigrations
# python3 manage.py migrate

# Gunicorn is better for production
gunicorn core.wsgi:application --bind 0.0.0.0:8000
